"use strict";
var Controller = require("locomotive").Controller,
    _ = require("underscore"),
    async = require("async");
//
var isPaginatedData = function (data) {
	return data.results && _.isNumber(data.count) && data.pages;
};
var transformErrorData = function (errorData, cb) {
	var res;
	if (_.isObject(errorData.errors)) {
		res = "";
		_.each(errorData.errors, function (val, prop) {
			res += val.message + " ";
		});
	}
	res = res || errorData.msg || errorData.message || errorData;
	cb(null, res);
};
var logUserFriendlyErrorMessage = function (errorMessage, cb) {
	console.log("Error happened");
	console.log(errorMessage);
	cb();
};
var transformItemsData = function (dataArr) {
	// todo maybe  should return a copy of done data
	var isArray = _.isArray(dataArr),
	    res = [];
	if (!isArray) {
		dataArr = [dataArr];
	}
	_.each(dataArr, function (data) {
		try {
			data = data.toJSON();
		} catch (e) {
		}
		_.each(data, function (val, prop) {
			var newPropName;
			if (prop.length >= 4 && prop.slice(0, 3) === "_id") {
				if (_.isObject(data[prop])) {
					newPropName = prop.replace("_id", "");
					// todo
					newPropName = newPropName.toLowerCase();
					data[newPropName] = data[prop];
					delete data[prop];
				}
			}
		});
		res.push(data);
	});
	return isArray ? res : res[0];
};
//
Controller.prototype.send = function (errData, doneData, pro) {
	var self = this;
	if (!errData && (_.isNull(doneData) || _.isUndefined(doneData))) {
		errData = "msgEmptyResponse";
	}
	if (!!errData) {
		async.waterfall(
			[
				async.apply(transformErrorData, errData),
				function (errDataTransformed, cb) {
					async.series(
						[
							async.apply(logUserFriendlyErrorMessage, errDataTransformed),
							function (cb) {
								cb(null, errDataTransformed);
							}
						],
						cb);
				},
				function (errDataTransformed, cb) {
					if (_.isArray(errDataTransformed)) {
						errDataTransformed = errDataTransformed[1];
					}
					cb(null, errDataTransformed);
				}
			],
			function (err, errDataTransformed) {
				if (!!err) {
					errDataTransformed = "msgErrorDuringErrorHandling";
				}
				self.res.statusCode = 400;
				self.res.send({
					msg: errDataTransformed
				});
			});
	}
	else {
		if (isPaginatedData(doneData)) {
			self.res.send({
				options: doneData.options,
				current: doneData.current,
				count:   doneData.count,
				items:   transformItemsData(doneData.results)
			});
		} else {
			self.res.send(transformItemsData(doneData));
		}
//
	}
};
Controller.prototype.check = function (filename) {
	return require("../middlewares/" + filename);
};
//
