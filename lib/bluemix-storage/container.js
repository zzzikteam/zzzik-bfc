"use strict";
var async = require("async"),
    request = require("request"),
    fs = require("fs"),
    mime = require("mime-types"),
    join = require("url-join");

var Container = function(opts = {}){
  this.name = opts.name;
  this.accessPoint = opts.accessPoint;
};
//
Container.prototype.download = function(filePath, token, cb){
  const tasks = [];
  tasks.push(cb => {
    var headers = {
        "X-Auth-Token": token
      },
      opts = { headers },
      url = join(this.accessPoint, this.name, filePath);
    console.log("Downloading", url);
    cb(null, request.get(url, opts));
  });
  async.waterfall(tasks, cb);
};

Container.prototype.upload = function(filePathFrom, filePathTo, token, cb){
  var dataForRequestTasks = {},
      makeRequestTasks = [];
    dataForRequestTasks.mimeType = function(cb){
      cb(null, mime.lookup(filePathFrom));
    };
    makeRequestTasks.push(async.apply(async.series.bind(async), dataForRequestTasks));
    makeRequestTasks.push((data, cb) => {
      var headers = {
            "X-Auth-Token": token,
            "Content-Type": data.mimeType
          },
          url = join(this.accessPoint, this.name, filePathTo),
      body = fs.createReadStream(filePathFrom),
      opts = { headers, url, body };
      cb(null, request.put(opts));
    });
    async.waterfall(makeRequestTasks, cb);
};

module.exports = Container;
