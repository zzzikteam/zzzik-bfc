"use strict";
var async = require("async"),
    AccessToken = require("./token"),
    Container = require("./container");
//
var Storage = function(opts){
  opts = opts || {};
  this.credentials = {
      "auth_url": "https://identity.open.softlayer.com",
      "project": "object_storage_e57cc2e2_b6d7_486d_89cb_7d8e4b5338c3",
      "projectId": "2b44e864418c4cdf9ba9ecf82ef2584f",
      "region": "london",
      "userId": "a99bd59453fb4aa199af9c1634115d16",
      "username": "Admin_b15e9c6c1cc951dfeffab5f02b108d6e7bb90295",
      "password": "RU0h-IiA{AJ&#fo8",
      "domainId": "98ab70027aa14a78be638fa5648cf4e9",
      "domainName": "1006071"
  };
  this.token = new AccessToken({ credentials: this.credentials });
};
//
Storage.prototype.getPublicAccessPoint = function(region){
  if (region === "dallas") {
    return "https://dal.objectstorage.open.softlayer.com";
  }
  if (region === "london") {
    return `https://lon.objectstorage.open.softlayer.com`;
  }
};
Storage.prototype.getPrivateAccessPoint = function(region) {
  var publicAccessPoint = this.getPublicAccessPoint(region);
  return `${publicAccessPoint}/v1/AUTH_${this.credentials.projectId}`;
};
Storage.prototype.proxy = function(opts = {}){
  var storage = this;
  return function(req, res, cb){
    var filePathFrom = req.params[0],
        container = req.params.container;
    storage.container(container).download(filePathFrom, function(err, stream){
      if(err) return cb(err);
      stream.pipe(res);
    });
  };
};
Storage.prototype.container = function(name, region = "london"){
  var accessPoint = this.getPrivateAccessPoint(region),
      container = new Container({
        name: name,
        accessPoint: accessPoint
      }),
      storage = this;
  return {
    temporize: function(name, cb){
      var tasks = [];
      tasks.push(async.apply(storage.token.get.bind(storage.token)));
      tasks.push(function(token, cb){
        cb(new Error("TODO"));
      });
      tasks.push(function(request, body, cb){
        cb(null, {
          key: ""
        });
      });
      async.waterfall(tasks, cb);
    },
    download: function(name, cb){
      var tasks = [];
      tasks.push(async.apply(storage.token.get.bind(storage.token)));
      tasks.push(async.apply(container.download.bind(container), name));
      async.waterfall(tasks, cb);
    },
    upload: function(filePathFrom, filePathTo, cb){
      var tasks = [];
      tasks.push(async.apply(storage.token.get.bind(storage.token)));
      tasks.push(async.apply(container.upload.bind(container), filePathFrom, filePathTo));
      async.waterfall(tasks, cb);
    }
  };
};



module.exports = Storage;
