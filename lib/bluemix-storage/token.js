"use strict";
var async = require("async"),
    request = require("request");

var AccessToken = function(opts = {}){
  this.credentials = opts.credentials;
  this.token = null;
  this.expiresAt = null;
  this.accessPoint = "https://identity.open.softlayer.com/v3/auth/tokens";
};

AccessToken.prototype.fetch = function(cb){
  var tasks = [],
      url = this.accessPoint,
      data = {
        "auth": {
          "identity": {
            "methods": [
              "password"
            ],
            "password": {
              "user": {
                id: this.credentials.userId,
                password: this.credentials.password
              }
            }
          },
          "scope": {
            "project": {
                "id": this.credentials.projectId
            }
          }
        }
      },
      opts = {
        json: true,
        body: data
      };
  tasks.push(async.apply(request.post.bind(request), url, opts));
  async.waterfall(tasks, cb);
};

AccessToken.prototype.get = function(cb){
  var self = this,
      tasks = [];
    if(this.isValid()){
      tasks.push(function(cb){
        cb(null, {
          token: self.token,
          expiresAt: self.expiresAt
        });
      });
    }else{
      tasks.push(async.apply(this.fetch.bind(this)));
      tasks.push((response, body, cb) => {
        var data = {
          token: response.headers["x-subject-token"],
          expiresAt: new Date(body.expires_at)
        };
        self.cache(data, function(err){
          cb(err, data);
        });
      });
    }
    tasks.push(function(data, cb){
      cb(null, data.token);
    });
  async.waterfall(tasks, cb);
};


AccessToken.prototype.isValid = function(){
  return !!this.expiresAt && new Date() < this.expiresAt;
};

AccessToken.prototype.cache = function(data, cb){
  this.token = data.token;
  this.expiresAt = data.expiresAt;
  cb();
};

module.exports = AccessToken;
