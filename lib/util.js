"use strict";
module.exports.isObjectId = function (id) {
	var re = new RegExp("^[0-9a-fA-F]{24}$");
	return re.test(id);
};