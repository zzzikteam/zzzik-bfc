var lcm = require("locomotive"),
    Controller = lcm.Controller,
    _ = require("underscore");
Controller.prototype.pickParams = function (propNames) {
	var res = {},
	    self = this,
	    val;
	_.each(propNames, function (propName) {
		val = self.req.param(propName);
		if (val === "true") {
			val = true;
		}
		if (val === "false") {
			val = false;
		}
		if (!_.isNull(val) && !_.isUndefined(val)) {
			res[propName] = val;
		}
	});
	return res;
};