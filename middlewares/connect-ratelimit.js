"use strict";
var _ = require("underscore"),
    cache = {},
    whitelist,
    blacklist,
    end = false,
    k = 10,
    config = {
	    whitelist: [
		    {
			    totalRequests: 5000,
			    every:         60 * 60 * 1000
		    }
	    ],
	    blacklist: [
		    {
			    totalRequests: 0,
			    every:         0
		    }
	    ],
	    normal:    [
		    {
			    totalRequests: 15 * k,
			    every:         1000 * 10
		    },
		    {
			    totalRequests: 45 * k,
			    every:         1000 * 60
		    },
		    {
			    totalRequests: 100 * k,
			    every:         1000 * 60 * 2
		    },
		    {
			    totalRequests: 150 * k,
			    every:         1000 * 60 * 60
		    },
		    {
			    totalRequests: 150 * k,
			    every:         1000 * 60 * 60 * 2
		    },
		    {
			    totalRequests: 250 * k,
			    every:         1000 * 60 * 60 * 24
		    }
	    ]
    },
    util = {
	    deepExtend: function (destination, source) {
		    for (var property in source) {
			    if (source[property] && source[property].constructor &&
				    source[property].constructor === Object) {
				    destination[property] = destination[property] || {};
				    util.deepExtend(destination[property], source[property]);
			    } else {
				    destination[property] = source[property];
			    }
		    }
		    return destination;
	    }
    };
function Client(name) {
	var self,
	    limits,
	    maxTime;
	this.setType();
	this.name = name;
	this.ticks = 1;
	this.dRegisteredAt = Date.now();
	//
	self = this;
	limits = config[this.type];
	//
	maxTime = _.chain(limits).pluck("every").max().value();
	setTimeout(function () {
		self.destroy();
	}, maxTime);
}
Client.prototype.destroy = function () {
	delete cache[this.name];
};
Client.prototype.isOk = function () {
	var self = this,
	    type = self.type,
	    now = Date.now();
	return _.every(config[type], function (limit) {
		if (now - self.dRegisteredAt < limit.every) {
			return self.ticks < limit.totalRequests;
		} else {
			return true;
		}
	});
};
Client.prototype.setType = function (name) {
	if (whitelist.indexOf(name) > -1) {
		this.type = "whitelist";
	}
	if (blacklist.indexOf(name) > -1) {
		this.type = "blacklist";
	}
	this.type = "normal";
};
function middleware(req, res, next) {
	var name = req.headers["x-forwarded-for"] || req.connection.remoteAddress,
	    client = cache[name],
	    isOk;
	if (req.url === "/favicon.ico") {
		next();
		return;
	}
	if (!client) {
		cache[name] = client = new Client(name);
	}
	isOk = client.isOk();
	if (isOk) {
		client.ticks++;
		next();
	} else {
		if (end === false) {
			next();
		} else {
			res.statusCode = 429;
			res.send({
				msg: "Sorry, toooo many requests from your IP."
			});
		}
	}
}
module.exports = function (options) {
	options = options || {};
	whitelist = options.whitelist || [];
	blacklist = options.blacklist || [];
	end = _.isBoolean(options.end) ? options.end : true;
	options.categories = options.categories || {};
	util.deepExtend(config, options.categories);
	return middleware;
};