"use strict";
module.exports = function(opts){
    return function(req, res, cb){
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
        res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Cache-Control, X-Requested-With");
        cb();
    };
};
