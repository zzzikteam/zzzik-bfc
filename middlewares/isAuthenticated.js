"use strict";
module.exports = function (req, res, next) {
	var msg = "";
	if (!!req.user && req.user.isAuthenticated()) {
		next();
	} else {
		// todo
		msg += "Authentication check failed";
		msg += "<br/><br/>";
		msg += "<a data-execute='auth'>";
		msg += "<button class='btn btn-xs btn-default'>";
		msg += "Authenticate via Facebook";
		msg += "</button>";
		msg += "</a>";
		next(new Error(msg));
	}
};