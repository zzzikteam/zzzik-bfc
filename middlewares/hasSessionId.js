"use strict";
module.exports = function (req, res, cb) {
	if (!!req.cookies && !!req.cookies["connect.sid"]) {
		cb();
	} else {
		// todo only string?
		cb(new Error("Session was not established but is required."));
	}
};