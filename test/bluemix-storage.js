"use strict";
var Storage = require("./lib/bluemix-storage/storage"),
    storage = new Storage(),
    fs = require("fs");

//
storage("bfc-pro").download("123123123/test.jpg", function (err, stream){
  stream.on("response", function(response){
    console.log("on stream response");
    console.log(response.statusCode);
    console.log(response.headers["content-type"]);
  }).pipe(fs.createWriteStream("doodle.jpg", "UTF-8"));
});

storage("bfc-pro").upload(`${__dirname}/public/img/1.jpg`, "/123123123/test.jpg", function(err, stream){
  console.log("upload res");
  console.log(err);

});
