module.exports = {
    root: "../",
    "shortcuts": true,
    log: true,
    "modules": {
        "$": {
            "exports": "$",
            "path": [
                "bower_components/jquery/dist/jquery.js",
                "bower_components/spinjs/spin.js",
                "bower_components/spinjs/jquery.spin.js",
                "bower_components/toastr/toastr.js",
                "client/local_components/modernizr-2.6.2-respond-1.1.0.min.js",
                "client/local_components/jquery.plugin.min.js",
                "client/local_components/bootstrap.js",
                "client/local_components/jquery.flexslider-min.js",
                "client/local_components/smooth-scroll.min.js",
                "client/local_components/skrollr.min.js",
                "client/local_components/spectragram.min.js",
                "client/local_components/scrollReveal.min.js",
                "client/local_components/isotope.min.js",
                "client/local_components/twitterFetcher_v10_min.js",
                "client/local_components/lightbox.min.js",
                "client/local_components/jquery.countdown.min.js",
                "client/local_components/snap.svg-min.js",
                "client/local_components/svgicons.js",
                "client/local_components/scripts.js"
            ]
        },
        "B": {
            "exports": "Backbone",
            "path": [
                "bower_components/backbone/backbone.js",
                "bower_components/backbone.syphon/lib/backbone.syphon.js",
                "bower_components/backbone-computedfields/lib/backbone.computedfields.js",
                "bower_components/backbone.select/dist/backbone.select.js",
                "bower_components/backbone.validation/dist/backbone-validation.js",
                "client/local_components/backbone.collection.filter.js"
            ],
            require: {
                "_": "_",
                "$": "$"
            }
        },
        "i118n": "client/i118n/ru.json",
        "M": {
            "exports": "Marionette",
            "path": "bower_components/marionette/lib/backbone.marionette.js",
            "require": {
                "Backbone": "B"
            }
        },
        "_": {
            path: "bower_components/underscore/underscore.js",
            exports: "_"
        },
        "util": "client/local_components/util.js",
        "yate": {
            "exports": "yr",
            "path": "bower_components/yate/lib/runtime.js"
        },
        "yate/runtime.js": "@yate",
        "yate/lib/runtime.js": "@yate"
    }
};
