module.exports = {
	root:    "../client",
	warn:    true,
	log:     true,
	node:    true,
	modules: {
		"${subdir}${file}": "**/*.js",
		main:               {
			"path": [
				"main.js",
				"tuner/**.js",
				"views/**.js",
				"controllers/**.js",
				"regions/**.js",
				"components/**/**.js",
				"behaviors/**/**.js",
				"entities/**.js",
				"apps/**/**.js"
			]
		},
		App:                "App.js"
	},
	main:    "main"
};