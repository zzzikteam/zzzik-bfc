"use strict";
var async = require("async");
module.exports = {
	requiredDowntime: false,
	up:               function (cb) {
		cb();
	},
	down:             function (cb) {
		throw new Error("irreversible migration");
	},
	test:             function (cb) {
		throw new Error("Is not supported. Yet.");
	}
};