var App = require("App"),
    toastr = window.toastr;
module.exports = App.module("Components.NotificationCenter", function (NC, App, B, M, $, _) {
    "use strict";
    var API = {
        showNotification: function (opts) {
            opts = opts || {};
            var type = opts.type || "success",
			    defaultsOpts = {
    "closeButton":     true,
    "debug":           false,
    "positionClass":   "toast-top-right",
    "onclick":         null,
    "showDuration":    "300",
    "hideDuration":    "1000",
    "timeOut":         "5000",
    "extendedTimeOut": "1000",
    "showEasing":      "swing",
    "hideEasing":      "linear",
    "showMethod":      "fadeIn",
    "hideMethod":      "fadeOut"
			    },
			    opts = _.extend(defaultsOpts, opts);
            toastr.options = opts;
            toastr[type](opts.msg, opts.title);
            toastr.options = defaultsOpts;
        }
    };
    App.commands.setHandler("notify", function (opts) {
        API.showNotification(opts);
    });
});
