"use strict";
var App = require("App"),
    $ = require("$");
$(window.document).on("click", "[data-navigate]", function (e) {
    var path = $(e.currentTarget).data("navigate");
    e.stopPropagation();
    e.preventDefault();
    if (path) {
        path.replace("#", "");
    }
    App.navigate(path, true);
});
$(window.document).on("click", "[data-execute]", function (e) {
    var path = $(e.currentTarget).data("execute");
    e.stopPropagation();
    e.preventDefault();
    App.execute(path, {});
});
