"use strict";
var App = require("App");
App.module("Components.Back", function (Components, App, B, M, $, _) {
    var stack = [],
    // todo
    isAllowedDublicates = false,
    API;
    //
    App.listenTo(B.history, "route", function (route, router, params) {
        App.vent.trigger("navigation:change:url", B.history.fragment);
    });
    App.navigate = function (route, opts) {
        var options,
		    defaults = {
    trigger: true,
    replace: false
		    };
        if (_.isBoolean(opts)) {
            options = {
                trigger: opts
            }
        } else {
            options = opts || {};
        }
        _.defaults(options, defaults);
        B.history.navigate(route, options);
        if (options.trigger === false) {
            App.vent.trigger("navigation:change:url", route, opts);
        }
    };
    App.getCurrentRoute = function () {
        return B.history.fragment;
    };
    //
    API = {
        getPreviousRoute: function () {
            var route;
            if (stack.length < 1) {
                route = App.request("default:route") || "/";
            } else {
                route = stack[stack.length - 2];
            }
            return route;
        }
    };
    // todo respect replace option
    App.commands.setHandler("navigation:navigate", function (route, opts) {
        App.navigate(route, opts);
    });
    App.vent.on("navigation:change:url", function (url, opts) {
        var routeCurrent = url,
        routeLast = stack.length > 0 ? stack[stack.length - 1 ] : null;
        if (isAllowedDublicates) {
            stack.push(routeCurrent);
        } else {
            if (routeCurrent !== routeLast) {
                stack.push(routeCurrent);
            }
        }
    });
    App.reqres.setHandler("navigation:previous", function () {
        return API.getPreviousRoute();
    });
});
