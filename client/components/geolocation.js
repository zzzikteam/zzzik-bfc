var App = require("App");
App.module("Components.Geolocation", function (Geolocation) {
    var getLocation = function (cb) {
        var nav = window.navigator;
        if (nav.geolocation) {
            nav.geolocation.getCurrentPosition(function (a, b) {
                if (a.coords) {
                    cb(null, {
                        lat: a.coords.latitude,
                        lng: a.coords.longitude
                    });
                } else {
                    cb({
                        error: "Error happened"
                    });
                }
            });
        } else {
            cb({
                error: "Error happened"
            });
        }
    };
    App.commands.setHandler("geolocation:run", function (cb) {
        getLocation(cb);
    });


    App.reqres.setHandler("geolocation:support", function () {
        return !!window.navigator.geolocation;
    });
});
