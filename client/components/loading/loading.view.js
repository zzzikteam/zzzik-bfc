var App = require("App");
App.module("Components.Loading", function (Loading, App, B, M, $, _) {
    "use strict";
    Loading.View = M.ItemView.extend({
        template:  false,
        className: "loading-container",
        onShow:    function () {
            var opts = this.getOpts();
            this.$el.spin(opts);
        },
        getOpts:   function () {
            return App.request("default:spin");
        },
        onDestroy: function () {
            this.$el.spin(false);
        }
    });
});
