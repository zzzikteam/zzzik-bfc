"use strict";
var B = window.Backbone,
    _ = window._;
B.FetchWithQuery = (function (Backbone, _) {
    var Query = B.Model.extend({

    });
    var FetchWithQuery = function (entity, params, QueryParam) {
        params = params || {};
        QueryParam = QueryParam || Query;
        this.entity = entity;
        this.query = new QueryParam(params);
        this.entity.query = this.query;
        this.initialize(entity);
    };
    FetchWithQuery.VERSION = "0.0.1";
    _.extend(FetchWithQuery.prototype, {
        DEBUG:       true,
        applyFilter: function () {
            // todo
        },
        initialize:  function () {
            var self = this,
            entity = self.entity,
            toJSON = self.query.toJSON.bind(self.query);
            var fetchOrigin = entity.fetch,
            xhr;
            entity.fetch = function (opts) {
                opts = opts || {};
                opts.data = opts.data || {};
                var data = toJSON({
                    query: true
                });
                // todo check if manual data (opts.data) does not override data from model (data)
                opts.data = _.extend(data, opts.data);
                xhr = fetchOrigin.call(entity, opts);
                entity.trigger("requested");
                return xhr;
            };
        }

    });
    return FetchWithQuery;
})(B, _);
