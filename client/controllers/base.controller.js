"use strict";
var App = require("App");
App.module("Controllers", function (Controllers, App, B, M, $, _) {
    Controllers.Base = M.Controller.extend({
        constructor:    function (opts) {
            opts = opts || {};
            this.region = opts.region || App.request("default:region");
            M.Controller.prototype.constructor.call(this, opts);
        },
        show:           function (view, opts) {
            opts = opts || {};
            _.defaults(opts, {
                loading: false,
                region:  this.region
            });
            this.setMainView(view);
            if (this.pageTitle) {
                this.setPageTitle();
            }
            this.manageMainView(view, opts);
        },
        setPageTitle:   function () {
            window.document.title = this.pageTitle + " / " + App.request("default:title");
        },
        setMainView:    function (view) {
            if (this.mainView) {
                return;
            }
            this.mainView = view;
            this.listenTo(view, "destroy", this.destroy);
        },
        manageMainView: function (view, opts) {
            if (opts.loading) {
                App.execute("show:loading", view, opts);
            } else {
                opts.region.show(view);
            }
        }
    });
});
