"use strict";
var App = require("App");
App.module("Regions", function (Regions, App, B, M, $, _) {
    Regions.Modal = M.Region.extend({
        el:                 "[data-region='modal']",
        skipNextRouteEvent: null,
        closeSelect2:       function () {
            // todo move to select2 logic
            // todo hack to close unclosed select2 boxes when showing popup
            // todo at least use more general class to close elems
            $("[name='dancers']").select2("close");
            $("[name='suggest-dancers']").select2("close");
        },
        onBeforeShow:       function (view) {
            this.closeSelect2();
        },
        onShow:             function (view) {
            this.listenTo(view, "destroy", this.navigateToRoute.bind(this, view));
            this.listenTo(App.vent, "navigation:change:url", this.onChangeURL);
            //
            this.skipNextRouteEvent = true;
            //
            view.$el.one("hidden.bs.modal", this.onModalHidden.bind(this, view));
            view.$el.modal("show");
        },
        onChangeURL:        function (url) {
            var view = this.currentView;
            if (this.skipNextRouteEvent === true) {
                this.skipNextRouteEvent = false;
                return;
            }
            if (!!view) {
                this.stopListening(view, "destroy");
                view.$el.modal("hide");
            }
        },
        onModalHidden:      function (view) {
            if (this.hasView()) {
                this.empty();
            }
            this.stopListening(App.vent, "navigation:change:url", this.onChangeURL);
        },
        navigateToRoute:    function (view) {
            console.log("navigating");
            // todo
            var route,
            routePrevious = App.request("navigation:previous"),
            routeDefault = view.getOption("defaultBackRoute"),
            trigger = !routePrevious;
            route = routePrevious || routeDefault;
            App.navigate(route, trigger);
        }
    });
});

