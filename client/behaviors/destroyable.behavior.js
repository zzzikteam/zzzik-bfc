"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.Destroyable = M.Behavior.extend({
        defaults: {
            prompt:  true,
            request: true
        },
        ui:       {
            destroy: "[data-action='destroy']"
        },
        events:   {
            "click @ui.destroy": "onClick"
        },
        onClick:  function (e) {
            var view = this.view,
            model = view.model,
            prompt = this.getOption("prompt"),
            request = this.getOption("request");
            e.preventDefault();
            e.stopPropagation();
            if ((!!prompt && window.confirm("Are you sure?")) || !prompt) {
                //view.$el.css("opacity", 0.5);
                if (request) {
                    model.destroy({
                        wait:   true,
                        always: function () {
                            view.$el.css("opacity", 1);
                        }
                    });
                } else {
                    this.view.destroy();
                }
            }
        }
    });
});
