"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.Validation = M.Behavior.extend({
        defaults:        {
            live: true
        },
        modelEvents:     {
        //"validated:invalid": "insertErrors"
        },
        events:          {

        },
        ui:              {
            inputs: "input"
        },
        onRender:        function () {
            var view = this.view;
            B.Validation.bind(view, {
                valid:   this.onModelValid.bind(this),
                invalid: this.onModelInvalid.bind(this)
            });
            this.view.$el.on("focusout", "input", this.onInputFocusOut.bind(this));
        },
        onDestroy:       function () {
            var view = this.view;
            B.Validation.unbind(view);
        },
        onModelValid:    function (view, propName) {
            this.clearErrors(propName);
        },
        onModelInvalid:  function (view, propName, error) {
            this.insertError(propName, error);
        },
        onInputFocusOut: function (e) {
            var $input = $(e.target),
            path = $input.attr("name"),
            val = $input.val(),
            error;
            error = this.view.model.preValidate(path, val);
            if (!!error) {
                this.insertError(path, error);
            } else {
                this.clearErrors(path);
            }
        },
        clearErrors:     function (propName) {
            var view = this.view,
            $formGroup = view.$el.find("input[name='" + propName + "']").closest(".form-group");
            $formGroup.removeClass("has-error");
            $formGroup.find(".validation-errors").empty();
        },
        insertError:     function (path, error) {

            var name = path,
            $formGroup = this.view.$el.find("input[name='" + name + "']").closest(".form-group"),
            $errors = $formGroup.find(".validation-errors");
            $formGroup.addClass("has-error");
            $errors.empty();
            $errors.append($("<div>").text(error));
        }
    });
});
