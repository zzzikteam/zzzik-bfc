"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.Selectable = M.Behavior.extend({

        ui:                {
            toggle: "[data-action='toggle-selected']"
        },
        events:            {
            "click @ui.toggle": "onClickToggle",
            "click":            "onClick",
            "mouseenter":       "setHoverClass",
            "mouseleave":       "removeHoverClass"
        },
        defaults:          {
            toggle:           true,
            className:        "list-item-selected",
            hasButtons:       true,
            classNameOnHover: "list-item-selectable"
        },
        modelEvents:       {
            "model:chosen":   "onModelSelected",
            "model:unchosen": "onModelDeselected",
            selected:         "onModelSelected",
            deselected:       "onModelDeselected"
        },
        setHoverClass:     function () {
            var view = this.view,
            className = this.getOption("classNameOnHover");
            view.$el.addClass(className);
        },
        removeHoverClass:  function () {
            var view = this.view,
            className = this.getOption("classNameOnHover");
            view.$el.removeClass(className);
        },
        onModelSelected:   function () {
            this.$el.addClass(this.options.className);
        },
        onModelDeselected: function () {
            this.$el.removeClass(this.options.className);
        },
        onClick:           function (e) {
            var hasButtons = this.getOption("hasButtons");
            if (!hasButtons) {
                e.preventDefault();
                e.stopPropagation();
                this.action();
            }
        },
        action:            function () {
            var toggle = this.getOption("toggle"),
            model = this.view.model;
            if (toggle) {
                model.toggleChoose();
            } else {
                model.choose();
                console.log("Model has been selected: " + model.id);
            }
        },
        onClickToggle:     function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.action();
        }
    });
});
