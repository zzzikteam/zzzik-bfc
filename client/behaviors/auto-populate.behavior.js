"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.AutoPopulate = M.Behavior.extend({
        defaults:          {
            mode:   "edit",
            fields: []
        },
        events:            {

        },
        isEditing:         function () {
            return this.getOption("mode") === "edit";
        },
        isInputIsDisabled: function (path) {
            var disabledInputs = this.getOption("disabled") || [];
            return _.contains(disabledInputs, path);
        },
        getInputType:      function (path) {
            var res = "text";
            if (path.match("bool_")) {
                res = "checkbox";
            }
            return res;
        },
        getFieldContainer: function (path, val, opts) {
            var $inputContainer,
            $input,
            $errors = $("<div class='validation-errors'></div>"),
            name,
            label,
            type,
            model = this.view.model;
            if (this.isEditing()) {
                name = App.request("convert:path:to:input:name", path);
                label = App.request("convert:path:to:input:label", model, path);
                type = this.getInputType(path);
                $input = $("<input class='form-control'>").attr("name", name);
                $input.attr("type", type);
                if (type === "text") {
                    $input.attr("value", val);
                }
                if (type === "checkbox" && val === true) {
                    $input.attr("checked", "checked");
                }
                if (this.isInputIsDisabled(path)) {
                    $input.attr("disabled", "disabled");
                }
                $inputContainer = $("<div class='form-group'>")
                .append($("<label class='control-label col-sm-3'>").text(label))
                .append($("<div class='col-sm-9'>").append($input).append($errors));
            }
            return $inputContainer;
        },
        onRender:          function () {
            var self = this,
            view = this.view,
            model = view.model,
            $container = view.$el.find("form"),
            fields = this.getOption("fields"),
            disabled = this.getOption("disabled");
            _.each(fields, function (path) {
                var propVal = self.getFieldValue(model, path);
                $container.append(self.getFieldContainer(path, propVal));
            });
            _.each(disabled, function () {
			});
        },
        getFieldValue:     function (model, path) {
            var res,
            attrs;
            res = model.get(path);
            if (!_.isBoolean(res) || !res) {
                attrs = path.split(".");
                if (attrs.length > 1) {
                    //
                    res = model.get(attrs[0]);
                    attrs = attrs.slice(1, attrs.length);
                    try {
                        _.each(attrs, function (attr) {
                            res = res[attr];
                        });
                    } catch (e) {
                        res = null;
                    }
                }
            }
            return res;
        }
    });
});
