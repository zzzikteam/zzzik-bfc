"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.SuccessReporter = M.Behavior.extend({
        defaults:    {
            msg: "Success!"
        },
        onRender:    function () {
            this.listenTo(this.view.model, "sync", this.onModelSync);
        },
        onModelSync: function () {
            App.execute("notify", {
                msg:  this.getOption("msg"),
                type: "success"
            });
        }
    });
});
