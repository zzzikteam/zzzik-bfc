"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.Rerender = M.Behavior.extend({
        defaults: {
            mode:   "edit",
            fields: []
        },
        ui:       {
            "rerender": "[data-action='rerender']"
        },
        events:   {
            "click @ui.rerender": "rerender"
        },
        rerender: function (e) {
            e.preventDefault();
            this.view.render();
        }

    });
});
