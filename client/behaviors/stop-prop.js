"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.StopProp = M.Behavior.extend({
        defaults: {
            selectors: [
            "a",
            "button",
            ".btn",
            "[data-action='stop-prop']",
            ".gm-style"
            ]
        },
        events:   {
            "click": "onClick"
        },
        onClick:  function (e) {
            var $target = $(e.target),
            $currentTarget = $(e.currentTarget),
            stop = false,
            selectors = this.getOption("selectors");
            //
            console.log($target);
            console.log($currentTarget);
            console.log(selectors);
            //
            stop = _.some(selectors, function (selector) {
                return $target.is(selector) || $currentTarget.is(selector);
            });
            if (stop) {
                e.stopPropagation();
            }
        }
    });
});
