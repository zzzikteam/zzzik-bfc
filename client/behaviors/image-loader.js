"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.ImageLoader = M.Behavior.extend({
        initialize:  function () {
            this.image = new Image();
        },
        renderImage: function (uiAttr, link) {
            var view = this.view,
            link = link,
            image = this.image,
            spinOpts = App.request("default:spin"),
            $container = view.ui[uiAttr],
            msg = "Image was not loaded properly",
            $text,
            $image;
            $container.empty();
            $container.spin(spinOpts);
            image.onerror = function () {
                $text = $("<p></p>").html(msg).addClass("text-muted").addClass("text-center");
                $container.spin(false);
                $container.html("");
                $container.append($text);
                $text.fadeOut(2500);
            };
            image.onload = function () {
                $container.spin(false);
                $container.html("");
                $image = $(image);
                $container.append($image);
            };
            image.src = null;
            image.src = link;
        },
        onRender:    function () {
            var self = this,
            model = this.view.model,
            opts = this.options,
            link;
            _.each(opts, function (linkAttr, uiAttr) {
                link = model.get(linkAttr);
                if (!!link) {
                    self.renderImage(uiAttr, link);
                }
            });
        },
        onDestroy:   function () {
            this.image.onload = null;
            this.image.onerror = null;
            delete this.image;
        }
    })
});
