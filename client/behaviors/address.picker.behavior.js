"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.AddressPicker = M.Behavior.extend({
        defaults:            {
            create:       false,
            paths:        {
                lat:     "lat",
                lng:     "lng",
                address: "address"
            },
            updateForm:   {
                latLng: true
            },
            updateModel:  {
                latLng:  true,
                address: true
            },
            providers:    [
            "autocomplete",
            "geocoder",
            "foursquare"
            ],
            notFoundText: "Ooops! Nothing found!"
        },
        events:              {
            "change @ui.address": "onChangeAddress"
        },
        ui:                  function () {
            return {
                address: "[name='" + this.getPath("address") + "']",
                lat:     "[name='" + this.getPath("lat") + "']",
                lng:     "[name='" + this.getPath("lng") + "']"
            };
        },
        onRender:            function () {
            this.ui.address.val(this.view.model.get("address"));
            this.renderAddressPicker();
        },
        onChangeAddress:     function () {
            var self = this,
            select2Data = self.ui.address.select2("data") || {},
            select2Model = select2Data.model,
            updateLatLng = this.getOption("updateForm").latLng,
			    opts = {
    success: function () {
        if (updateLatLng) {
            self.ui.lng.val(select2Model.get("lng"));
            self.ui.lat.val(select2Model.get("lat"));
            self.ui.lng.trigger("change");
            self.ui.lat.trigger("change");
            self.updateModel();
        }
    }
			    };
            if (!_.isNumber(select2Data.lat) || !_.isNumber(select2Data.lng)) {
                select2Model.fetch(opts);
            } else {
                opts.success();
            }
        },
        getPath:             function (name) {
            var paths = this.getOption("paths");
            return paths[name];
        },
        updateModel:         function () {
            var data = {},
            model = this.view.model,
            updateModelOpts = this.getOption("updateModel");
            //
            if (updateModelOpts.latLng) {
                data[this.getPath("lat")] = window.parseFloat(this.ui.lat.val());
                data[this.getPath("lng")] = window.parseFloat(this.ui.lng.val());
            }
            if (updateModelOpts.address) {
                data[this.getPath("address")] = this.ui.address.val();
            }
            //
            data = _.compactObject(data);
            model.set(data);
            if (model.changed && (model.changed.lat || model.changed.lng)) {
                model.trigger("change:lat'n'lng", model, {
                    lat: model.get("lat"),
                    lng: model.get("lng")
                });
            }
        },
        renderAddressPicker: function () {
            var self = this,
            notFoundText = this.getOption("notFoundText"),
            providers = this.getOption("providers"),
            create = this.getOption("create"),
			    opts = {
    placeholder:        "Выберите адрес",
    initSelection:      function (elem, cb) {
        var val = $(elem).val();
        cb({
            id:   val,
            text: val
        });
    },
    nextSearchTerm:     function (selectedObject, currentSearchTerm) {
        return selectedObject.text || currentSearchTerm || "";
    },
    minimumInputLength: 2,
    formatNoMatches:    function () {
        return notFoundText;
    },
    query:              function (query) {
        var select2Data = {
            results: []
        },
        entities;
        entities = _.map(providers, function (provider) {
            if (provider === "autocomplete") {
                return App.request("address:google", {
                    query: query.term,
                    type:  "autocomplete"
                });
            }
            if (provider === "foursquare") {
                return App.request("address:foursquare", {
                    query: query.term
                });
            }
            if (provider === "geocoder") {
                return App.request("address:google", {
                    query: query.term,
                    type:  "geocoder"
                });
            }
        });
        App.execute("when:fetched", entities, function () {
            _.each(entities, function (addresses) {
                if (addresses.length > 0) {
                    select2Data.results.push({
                        text:     addresses.getSelect2Title(),
                        children: addresses.getSelect2Data()
                    });
                }
            });
            query.callback(select2Data);
        });
    }
			    };
            if (create) {
                opts.createSearchChoice = function (term) {
                    return {
                        id:   term,
                        text: term
                    };
                }
            }
            self.ui.address.select2(opts);
            // todo
            self.ui.address.on("select2-selecting", function () {
                self.ui.address.one("change", function () {
                    self.ui.address.select2("open");
                })
            });
            //
            self.ui.address.on("change", self.onChangeAddress.bind(self));
        }
    })
});
