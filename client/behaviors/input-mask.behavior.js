"use strict";
var App = require("App");
App.module("Behaviors", function (Behaviors, App, B, M, $, _) {
    Behaviors.InputMask = M.Behavior.extend({
        defaults:  {
        },
        events:    {

        },
        onRender:  function () {
            var opts = this.options;
            _.each(opts, this.maskInput.bind(this));
        },
        maskInput: function (propVal, propName) {
            var view = this.view,
            $input;
            propName = propName.replace("[", "\\[").replace("]", "\\]");
            $input = view.$el.find("[name={name}]".replace("{name}", propName));
            $input.mask(propVal);
        }
    });
});
