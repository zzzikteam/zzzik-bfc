"use strict";
var App = require("App");
App.module("Entities", function (Entities, App, B, M, $, _) {
    var moment = require("moment"),
    API;
    var isTime = function (t) {
        if (!t) {
            return false;
        }
        if (!t.match(/\d\d:\d\d/)) {
            return false;
        }
        var numbers = t.split(":");
        if (numbers.length !== 2) {
            return false;
        }
        if (numbers[0] < 0 || numbers[0] > 24) {
            return false;
        }
        if (numbers[1] < 0 || numbers[1] > 60) {
            return false;
        }
        return true;
    };
    var validateSchedule = function (val, attr) {
        if (!val) {
            if (attr.match(/schedule\[\d/) && this.get("schedule")) {
                var pos = attr.match(/\d/)[0];
                val = this.get("schedule")[pos];
            }
        }
        if (!val) {
            return "";
        }
        if (val === "Выходной") {
            return "";
        }
        var values = val.split(" - ");
        if (values.length !== 2) {
            return "Нужно указать время начала и время окончания работы";
        } else {
            if (!isTime(values[0])) {
                return "Время начала работы указано неправильно";
            }
            if (!isTime(values[1])) {
                return "Время окончания работы указано неправильно";
            }
        }
    };
    Entities.ServicePoint = B.Model.extend({
        validation:   {
            "schedule.0":  validateSchedule,
            "schedule.1":  validateSchedule,
            "schedule.2":  validateSchedule,
            "schedule.3":  validateSchedule,
            "schedule.4":  validateSchedule,
            "schedule.5":  validateSchedule,
            "schedule.6":  validateSchedule,
            "schedule[0]": validateSchedule,
            "schedule[1]": validateSchedule,
            "schedule[2]": validateSchedule,
            "schedule[3]": validateSchedule,
            "schedule[4]": validateSchedule,
            "schedule[5]": validateSchedule,
            "schedule[6]": validateSchedule,
            name:          {
                required: true
            },
            lat:           {
                required: true
            },
            lng:           {
                required: true
            },
            transportType: {
                required: true
            },
            brands:        {
                fn: function (value) {
                    return (!!value && value.length > 0) ? "" : "Brand is required";
                }
            },
            address:       {
                required: true
            }
        },
        idAttribute:  "_id",
        defaults:     {},
        urlRoot:      "/api/v1/service-points",
        initialize:   function () {
            this.computedFields = new B.ComputedFields(this);
            B.Select.Me.applyTo(this);
        },
        isPrimeBrush: function () {
            return this.get("brand") === "primebrush";
        }
    });
    Entities.ServicePointsQuery = B.Model.extend({
        computed:   {
            isSelected: {
                depends: [
					function (cb) {
    this.on("selected", cb);
    this.on("deselected", cb);
					}
				],
                get:     function () {
                    return this.selected;
                }
            },
            isActive:   {
                depends: [
                "tags",
                "type"
                ],
                get:     function () {
                    return !_.isEmpty(this.pick(this.computed.isActive.depends));
                }
            }
        },
        initialize: function () {
            B.Select.Me.applyTo(this);
            //
            this.computedFields = new B.ComputedFields(this);
        },
        toJSON:     function (opts) {
            opts = opts || {};
            var res;
            if (opts.query === true) {
                res = _.clone(_.pick(this.attributes, [
                "brands",
                "transportType"
                ]));
                if (res.brands) {
                    res.brands = {
                        $in: [
                        res.brands,
                        "other"
                        ]
                    };
                }
            } else {
                res = _.clone(this.attributes);
            }
            return res;
        }
    });
    Entities.ServicePoints = B.Collection.extend({
        url:        "/api/v1/service-points",
        model:      Entities.ServicePoint,
        initialize: function (models, opts) {
            opts = opts || {};
            B.Select.One.applyTo(this, models);
            new B.FetchWithQuery(this, opts.query, Entities.ServicePointsQuery);
        }
    });
    //
    API = {
        getCollection: function (opts) {
            var collection = new Entities.ServicePoints([], opts);
            collection.fetch();
            return collection;
        },
        getModel:      function (opts) {
            var id = opts.id,
            model;
            if (!!id) {
                if (Entities.ServicePoint.findOrCreate) {
                    model = Entities.ServicePoint.findOrCreate({
                        _id: id
                    });
                } else {
                    model = new Entitites.ServicePoint({
                        _id: id
                    });
                }
            } else {
                model = new Entities.ServicePoint(opts.attrs);
            }
            if (opts.fetch === true) {
                model.fetch();
            }
            return model;
        }
    };
    //
    App.reqres.setHandler("service:points", function (opts) {
        return API.getCollection(opts);
    });
    //
    App.reqres.setHandler("service:point", function (id, opts) {
        if (_.isString(id) || _.isNumber(id)) {
            opts = opts || {};
            opts.id = id;
        } else {
            opts = id;
        }
        return API.getModel(opts);
    });
});
