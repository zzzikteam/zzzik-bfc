"use strict";
var M = require("M"),
    yr = require("yate"),
    _ = require("_");
//
//
M.Behaviors.behaviorsLookup = function () {
    var App = require("App");
    return App.Behaviors;
};
//
M.Renderer.render = function (templateName, data) {
    if (typeof templateName === "function") {
        return templateName(data);
    }
    // todo maybe remove
    return yr.run(templateName, data);
};
// todo check
var con = M.CollectionView.prototype.constructor;
M.CollectionView = M.CollectionView.extend({
    constructor:     function (opts) {
        var collection;
        collection = !!opts && opts.collection ? opts.collection : null;
        if (collection && !!this.loading) {
            this.listenTo(collection, "request", this.showLoadingView);
            this.listenTo(collection, "sync", this.hideLoadingData);
            this.listenTo(collection, "destroy", this.hideLoadingData);
        }
        return con.call(this, opts);
    },
    showLoadingView: function () {
        if (this.loading.type === "opacity") {
            // todo
            //this.$el.find("*").attr("disabled", "disabled").off("click");
            return this.$el.css("opacity", 0.5);
        }
        if (this.loading === true || this.loading.type === "spin") {
            this.collection.reset();
            this.$el.empty();
            var App = require("App"),
            spinOpts = _.extend(App.request("default:spin"), this.loading.opts || {});
            return this.$el.spin(spinOpts);
        }
        return;
    },
    hideLoadingData: function () {
        if (this.loading.type === "opacity") {
            return this.$el.css("opacity", 1);
        }
        if (this.collection.length === 0) {
            this.collection.reset();
        }
        if (this.loading === true || this.loading.type === "spin") {
            return this.$el.spin(false);
        }
        return;
    }
});
M.TemplateCache.loadTemplate = function (templateId) {
    require("template/" + templateId);
};
