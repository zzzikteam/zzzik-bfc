"use strict";
var $ = require("$");
$.fn.serializeObject = function () {
    var o = {},
    a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || "");
        } else {
            o[this.name] = this.value || "";
        }
    });
    return o;
};
$.fn.isOnScreen = function () {
    var $win = $(window),
	    viewport = {
    top:  $win.scrollTop(),
    left: $win.scrollLeft()
	    };
    viewport.right = viewport.left + window.document.body.clientWidth;
    viewport.bottom = viewport.top + window.document.body.clientHeight;
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};
$.tunintEnabled = true;
