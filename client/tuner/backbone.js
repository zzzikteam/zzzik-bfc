"use strict";
var B = require("B"),
    _ = require("_"),
    methodMap = {
        "create": "POST",
        "update": "PUT",
        "patch":  "PATCH",
        "delete": "DELETE",
        "read":   "GET"
    },
    sync = function (method, model, options) {
        var type = methodMap[method];
        // Default options, unless specified.
        _.defaults(options || (options = {}), {
            emulateHTTP: B.emulateHTTP,
            emulateJSON: B.emulateJSON
        });
        // Default JSON-request options.
        var params = {type: type, dataType: "json"};
        // Ensure that we have a URL.
        if (!options.url) {
            params.url = _.result(model, "url") || urlError();
        }
        // Ensure that we have the appropriate request data.
        if (options.data == null && model && (method === "create" || method === "update" || method === "patch")) {
            params.contentType = "application/json";
            params.data = JSON.stringify(options.attrs || model.toJSON(options));
        }
        // For older servers, emulate JSON by encoding the request into an HTML-form.
        if (options.emulateJSON) {
            params.contentType = "application/x-www-form-urlencoded";
            params.data = params.data ? {model: params.data} : {};
        }
        // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
        // And an `X-HTTP-Method-Override` header.
        if (options.emulateHTTP && (type === "PUT" || type === "DELETE" || type === "PATCH")) {
            params.type = "POST";
            if (options.emulateJSON) params.data._method = type;
            var beforeSend = options.beforeSend;
            options.beforeSend = function (xhr) {
                xhr.setRequestHeader("X-HTTP-Method-Override", type);
                if (beforeSend) return beforeSend.apply(this, arguments);
            };
        }
        // Don"t process data on a non-GET request.
        if (params.type !== "GET" && !options.emulateJSON) {
            params.processData = false;
        }
        // If we"re sending a `PATCH` request, and we"re in an old Internet Explorer
        // that still has ActiveX enabled by default, override jQuery to use that
        // for XHR instead. Remove this line when jQuery supports `PATCH` on IE8.
        if (params.type === "PATCH" && noXhrPatch) {
            params.xhr = function () {
                return new ActiveXObject("Microsoft.XMLHTTP");
            };
        }
        // Make the request, allowing the user to override any Ajax options.
        var xhr = options.xhr = B.ajax(_.extend(params, options));
        model.trigger("request", model, xhr, options);
        return xhr;
    };
// todo use everywhere
B.API_ROOT = "/api/v1";
B.sync = function (method, entity, opts) {
    var App = require("App"),
    xhr,
    url = _.isFunction(entity.url) ? entity.url() : entity.url,
    baseUrl = "",
    error = opts.error,
	    notify = function (xhr, statusText, thrown) {
    var serveResponse = xhr.responseText,
    msg,
    serverResponseInJSON;
    try {
        serverResponseInJSON = JSON.parse(serveResponse);
        msg = typeof serverResponseInJSON.msg === "string" ? serverResponseInJSON.msg : statusText;
    } catch (e) {
        msg = "Default client error message";
    }
    App.execute("notify", {
        type: "error",
        msg:  msg
    });
	    };
    if (url) {  // If no url, don't override, let B.sync do its normal fail
        opts = opts || {};
        opts.url = baseUrl + url;
    }
    if (_.isNull(opts.timeout)) {
        opts.timeout = 10000;
    }
    opts.error = function (xhr, statusText, thrown) {
        if (!!error) {
            error(xhr, statusText, thrown);
        }
        if (statusText === "timeout") {
            App.execute("notify", {
                type: "error",
                msg:  "Server does not response :-("
            });
        } else {
            if (statusText === "error" && xhr.readyState !== 4) {
                App.execute("notify", {
                    type: "error",
                    msg:  "Ooopsy-doopsy! Something wrong with the server. <br/><br/>Please reload."
                });
            } else {
                notify(xhr, statusText, thrown);
            }
        }
    };
    xhr = sync.apply(this, [method, entity, opts]);
    entity._xhr = xhr;
    return xhr;
};
var getId = function (prop) {
    var propNameWithId;
    if (!!this.get(prop)) {
        return this.get(prop)._id;
    }
    propNameWithId = "_id" + prop.slice(0, 1).toUpperCase() + prop.slice(1, prop.length);
    return this.get(propNameWithId);
};
B.Model.prototype.getId = getId;
B.Model.prototype.isSelected = function () {
    return !!this.selected;
};

B.Collection.prototype.selectById = function (id) {
    var model = this.get(id);
    if (!!model) {
        model.select();
    }
};
B.Collection.prototype.getSelected = function () {
    return this.find(function (model) {
        return model.selected;
    });
};
