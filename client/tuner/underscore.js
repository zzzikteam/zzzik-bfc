"use strict";
var _ = require("_");
_.mixin({
    compactObject: function (o) {
        var clone = _.clone(o);
        _.each(clone, function (v, k) {
            if (!v)
            delete clone[k];
        });
        return clone;
    },
    findIndex:     function (arr, val, context) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                return i;
            }
        }
    }
});


