"use strict";
var App = require("App");
App.module("UseCases", function(UseCases, App, B, M, $, _) {
    var Router,
        API;
    //
    Router = M.AppRouter.extend({
        appRoutes: {
            "/use-cases-disabled": "list",
            "use-cases-disabled": "list"
        }
    });
    API = {
        list: function() {
            new UseCases.List.Controller();
        },
        show: function() {
            new UseCases.Show.Controller();
        }
    };
    new Router({
        controller: API
    });
    //
    $(".blog-filters-menu > li").click(function(elem) {
        var $li = $(elem.target),
            key = $li.attr("data-filter").replace(".", ""),
            $empty = $(".sub-menu-container-empty"),
            $subs = $(".sub-menu-container");

        console.log("Filtering");
        $subs.removeClass("hidden");
        $subs.hide();
        if (key === "*") {
            $empty.show();
        } else {
            $(".sub-menu-container-" + key).show();
            $(".sub-menu-container-" + key).find("li").removeClass("active");
            $($(".sub-menu-container-" + key).find("li")[0]).addClass("active");
        }


    });
    //
});
