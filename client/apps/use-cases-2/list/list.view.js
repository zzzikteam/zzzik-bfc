"use strict";
var App = require("App");
App.module("UseCases.List", function(List, App, B, M, $, _) {
    var templateRootPath = "apps/use-cases/list/templates";
    List.Layout = M.LayoutView.extend({
        template: templateRootPath + "/layout"
    });
});
