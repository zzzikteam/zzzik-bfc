"use strict";
var App = require("App");
App.module("UseCases.Show", function(Show, App, B, M, $, _) {
    var templateRootPath = "apps/use-cases/show/templates";
    Show.Layout = M.LayoutView.extend({
        template: templateRootPath + "/layout"
    });
});
