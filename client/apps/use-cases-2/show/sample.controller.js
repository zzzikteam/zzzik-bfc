"use strict";
var App = require("App");
App.module("UseCases.Show", function(Show, App, B, M, $, _) {
    Show.Controller = App.Controllers.Base.extend({
        initialize: function(opts) {
            var layout = new Show.Layout();
            this.show(layout);
        }
    });
});
