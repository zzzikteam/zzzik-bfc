"use strict";
var App = require("App");
App.module("Sample", function (Sample, App, B, M, $, _) {
    var Router,
    API;
    Router = M.AppRouter.extend({
        appRoutes: {
            "sample/list": "list",
            "sample/show": "show"
        }
    });
    API = {
        list: function () {
            new Sample.List.Controller();
        },
        show: function () {
            new Sample.Show.Controller();
        }
    };
    new Router({
        controller: API
    });
});
