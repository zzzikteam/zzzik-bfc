"use strict";
var App = require("App"),
    templateRootPath = "apps/sample/list/templates";
App.module("Sample.List", function(List, App, B, M, $, _) {
    List.Layout = M.LayoutView.extend({
        template: templateRootPath + "/layout"
    });
});
