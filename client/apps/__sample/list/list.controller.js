"use strict";
var App = require("App");
App.module("Sample.List", function(List, App, B, M, $, _) {
    List.Controller = App.Controllers.Base.extend({
        initialize: function(opts) {
            var layout = new List.Layout();
            this.show(layout);
        }
    });
});
