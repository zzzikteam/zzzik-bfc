"use strict";
var App = require("App"),
    templateRootPath = "apps/sample/show/templates";
App.module("Sample.Show", function (Show, App, B, M, $, _) {
    Show.Layout = M.LayoutView.extend({
        template: templateRootPath + "/layout"
    });
});
