"use strict";
var B = require("B"),
    M = require("M"),
    App = new M.Application(),
    $ = require("$"),
    _ = require("_");
//
App.commands.setHandler("scroll", function($el, time) {
    var defaultTime = 600;
    time = _.isNumber(time) ? time : defaultTime;
    $("html, body").animate({
        scrollTop: $el.offset().top - 45
    }, time);
});
App.commands.setHandler("when:fetched", function(entities, cb) {
    // todo handle failed response possibility
    var xhrArr = _.chain([entities]).flatten().pluck("_xhr").value();
    return $.when.apply($, xhrArr).done(function() {
        cb();
    });
});

App.reqres.setHandler("default:region", function() {
    return App.content;
});
App.reqres.setHandler("default:title", function() {
    return "BFC";
});
App.reqres.setHandler("default:spin", function() {
    return {
        lines: 10,
        length: 6,
        width: 2.5,
        radius: 7,
        corners: 1,
        rotate: 9,
        direction: 1,
        color: "#000",
        speed: 1,
        trail: 60,
        shadow: false,
        hwaccel: true,
        className: "spinner",
        zIndex: 2e9,
        top: "50%",
        left: "50%"
    };
});
App.reqres.setHandler("default:route", function() {
    return "";
});
App.reqres.setHandler("convert:path:to:input:label", function(model, path) {
    var labels = model.labels;
    if (labels) {
        return labels[path] || path;
    } else {
        return path;
    }
});
//
App.addInitializer(function() {
    App.addRegions({
        content: "[data-region='content']",
        modal: App.Regions.Modal,
        menu: "[data-region='navbar']"
    });
});
App.addInitializer(function() {
    var hash = window.location.hash;
    if (!hash || hash === "") {
        window.location.hash = App.request("default:route");
    }
});
App.addInitializer(function(opts) {
    // todo speed up
    B.history.start({
        pushState: true,
        root: "../"
    });
});
window.App = App;
module.exports = App;
