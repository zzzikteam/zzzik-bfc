module.exports = {
    web: {
        project: "bfc"
    },
    project: "bfc",
    cookie: {
        secret: "test_secret"
    },
    mongoose: {
        debug: {
            isEnabled: false,
            format: null
        }
    },
    templateData: {
        theme: "matte"
    },
    ui: {
        useCasesMenu: [{
            id: "body",
            name: "Уход за телом",
            sub: [{
                id: "bathhealth",
                name: "Для ванной и душа"
            }, {
                id: "gigiena",
                name: "Гигиена"
            }, {
                id: "kozha",
                name: "Уход за кожей"
            }]
        }, {
            id: "house",
            name: "Уход за домом",
            sub: [{
                    id: "house",
                    name: "Чистый дом"
                }, {
                    id: "kitchen",
                    name: "Кухня"
                }, {
                    id: "text",
                    name: "Текстиль и стирка"
                }, {
                    id: "bathroom",
                    name: "Ванная комната"
                }, {
                    id: "green",
                    name: "Растения"
                }, {
                    id: "street",
                    name: "Уборка на улице"
                }

            ]
        }, {
            id: "animals",
            name: "Уход за животными",
            sub: [{
                id: "pitomez",
                name: "Уход за питомцем"
            }, {
                id: "health",
                name: "Здоровье"
            }, {
                id: "animalgigiena",
                name: "Уборка и гигиена"
            }, {
                id: "aqua",
                name: "Очистка аквариумов"
            }]
        }, {
            id: "business",
            name: "Продукты для бизнеса",
            sub: [{
                id: "office",
                name: "Офис"
            }, {
                id: "proizvodstvo",
                name: "Производство"
            }, {
                id: "horeka",
                name: "HORECA"
            }, {
                id: "torgovlya",
                name: "Торговля"
            }, {
                id: "bustransport",
                name: "Транспорт"
            }, {
                id: "avia",
                name: "Авиация"
            }, {
                id: "med",
                name: "Медицина"
            }, {
                id: "uncitydick",
                name: "Сельское хозяйство"
            }]
        }, {

            id: "transport",
            name: "Транспорт",
            sub: [{
                id: "garage-prost",
                name: "Гаражное пространство"
            }, {
                id: "otdel-obezh",
                name: "Отделители и обезжиривали"
            }, {
                id: "mouka-uhod",
                name: "Мойка и уход"
            }, {
                id: "obsluzh",
                name: "Обслуживание"
            }]
        }, {
            id: "mir-bakteriy",
            name: "Мир бактерий",
            sub: [{
                id: "mir-1",
                name: "1"
            }, {
                id: "mir-2",
                name: "2"
            }, {
                id: "mir-3",
                name: "3"
            }]
        }, {
            id: "ustroystva",
            name: "Устройства",
            sub: [{
                "id": "krasava",
                name: "Красота"
            }, {
                id: "zdorove-okrug",
                name: "Здоровое окружение"
            }]
        }]
    }
};
