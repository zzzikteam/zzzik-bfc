"use strict";
var express = require("express"),
    util = require("util"),
    MongoStore = require("connect-mongo")(express),
    config = require("config"),
    limiter = require("../../middlewares/connect-ratelimit"),
    handlebars = require("express-handlebars"),
    cors = require("../../middlewares/cors"),
    Storage = require("../../lib/bluemix-storage"),
    storage = new Storage();

module.exports = function() {
    //
    this.set("views", __dirname + "/../../app/views");
    this.engine("hbs", handlebars({
        defaultLayout: "main",
        layoutsDir: "app/views/bfc/layouts",
        partialsDir: "app/views/bfc/partials",
        extname: ".hbs"

    }));
    this.set("view engine", "hbs");
    //
    this.use(express.methodOverride());
    this.use(express.compress());
    this.use(express.static(__dirname + "/../../public"));
    //
    this.use(express.cookieParser());
    /*
    this.use(express.session({
        secret: config.cookie.secret,
        store:  new MongoStore({
            url: config.dbConnectionString
        })
    }));
*/
    this.use(limiter({
        end: true
    }));
    this.use(express.bodyParser());
    this.use(cors());
    this.get("/store/:container/*", storage.proxy());
    this.use(this.router);
    this.use(function(err, req, res, next) {
        if (!err) {
            next();
        } else {
            console.log(err);
            console.log(err.trace);
            console.log(err.stack);
            res.send({
                msg: err.msg || err.message || "Default server error message"
            }, 503);
        }
    });
};
