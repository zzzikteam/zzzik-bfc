"use strict";
var mongoose = require("mongoose"),
    config = require("config");
//
module.exports = function(cb) {
    mongoose.connect(config.dbConnectionString, function(err, res) {
        if (err) {
            throw new Error(err);
        }
        var db = mongoose.connection.db;
        db.command({
            buildInfo: true
        }, function(err, result) {
            if (err) throw err;
            console.log("Running mongodb. Version: " + result.version);
            if (config.mongoose.debug.isEnabled) {
                mongoose.set("debug", config.mongoose.debug.format);
            }
        });
        cb();
    });
};
