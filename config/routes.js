"use strict";
module.exports = function routes() {

    // COMMON
    this.root("pages#index");
    
    this.namespace("api", function() {
        this.namespace("v1", function() {
            this.resource("email");
            this.resources("menu");
            this.resources("products");
        });
    });
    //
    this.match("/404", "pages#notFound");
    this.match("/about", "pages#about");
    this.match("/contact", "pages#contact");
    this.match("/products", "pages#products");
    // BFC SPECIFIC
    this.match("/innovation", "pages#innovation");
    this.match("/GPD_safe_food", "pages#gPDSafeFood");
    this.match("/farm", "pages#farm");
    this.match("/production_distribution", "pages#productionDistribution");
    this.match("/gpd-3", "pages#gpd3");
    this.match("/news", "pages#news");
    this.match("/buy", "pages#buy");
    this.match("/med", "pages#med");
    this.match("/acsg", "pages#acsg");
    this.match("/article", "pages#article");
    this.match("/product", "pages#product");
    this.match("/natural-make-up", "pages#naturalMakeUp");
    this.match("/safety", "pages#safety");
    this.match("/products-of-cleaning", "pages#productsOfCleaning");
    this.match("/problem-solving", "pages#problemSolving");
    this.match("/probiotic", "pages#probiotic");
    this.match("/body_care", "pages#bodyCare");
    this.match("/duo", "pages#duo");
    this.match("/animal_care", "pages#animalCare");
    this.match("/air-clean", "pages#airClean");
    this.match("/cleaning-components", "pages#cleaningComponents");
    this.match("/directions", "pages#directions");
    this.match("/home_cleaning", "pages#homeCleaning");
    this.match("/professional", "pages#professional");
    this.match("/pro_active", "pages#proActive");
    this.match("/private", "pages#private");
    this.match("/horeca", "pages#horeca");

    //
    this.resources("use-cases");
    //
    this.match("*", "pages#notFound");
};
