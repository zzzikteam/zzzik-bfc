# Развертывание

- Установить node
- Установить mongod
- Запустить монгу (mongod --fork)
- Установить bower
- Установить grunt
- Скачать (обновить) репозиторий
- Переключиться на develop ветку
- Выполнить npm install
- Выполнить bower install
- Выполнить grunt
- http://localhost:3000
- тест