"use strict";
var _ = require("underscore"),
    config = require("config");

//
module.exports = function(grunt) {
    var optionsForMongooseMigrations = {
        path: process.cwd() + "/migrations",
        mongo: config.dbConnectionString,
        template: grunt.file.read("./migrations/__template.js"),
        ext: "js"
    };
    var optionsForClean = {
        yate: [
            "client/apps/*/*/templates/*.js",
            "client/apps/*/*/templates_source/*.obj",
            "client/apps/*/*/templates_source/*.ast",
            "client/apps/*/*/templates/*.ast",
            "client/components/*/templates/*.js",
            "client/components/*/templates_source/*.obj",
            "client/components/*/templates_source/*.ast",
            "client/components/*/templates/*.ast",
            "client/components/*/templates/*.obj"
        ]
    };
    var optionsForYate = {
        options: {
            writeAST: false
        },
        user: {
            files: [{
                src: [
                    "client/apps/**/templates_source/*.yate",
                    "client/components/**/templates_source/*.yate"
                ],
                expand: true,
                rename: function(dest, src) {
                    src = src.replace("templates_source", "templates");
                    src = src.replace(".yate", ".js");
                    return src;
                }
            }]
        }
    };
    var optionsForWatch = {
        "options": {
            livereload: false
        },
        //
        "custom.less": {
            files: [
                "less/**.less"
            ],
            tasks: [
                "less"
            ]
        },
        //
        "client.js": {
            files: [
                "client/**/*.js",
                "client/**/*.json",
                ".lmd/*.json"
            ],
            tasks: ["lmd:user_develop"]
        },
        "client.yate": {
            files: [
                "client/components/**/**.yate",
                "client/apps/**/**.yate"
            ],
            tasks: [
                "yate"
            ]
        }
    };
    var optionsForNodemon = {
        options: {
            ext: "js",
            logConcurrentOutput: true,
            watch: [
                "app",
                "config",
                "lib",
                "middlewares"
            ]
        },
        develop: {
            script: "server.js",
            options: {
                exec: "./node_modules/babel-cli/bin/babel-node.js",
                logConcurrentOutput: true,
                env: {
                    NODE_ENV: "develop"
                }
            }
        },
        production: {
            script: "server.js",
            options: {
                logConcurrentOutput: true,
                env: {
                    NODE_ENV: "production"
                }
            }
        },
        debug: {
            script: "server.js",
            options: {
                nodeArgs: ['--debug'],
                env: {
                    NODE_ENV: "test"
                }
            }
        }
    };
    var optionsForLmd = {
        // todo
        "user_develop": {
            options: {
                output: "public/js/build.user.js",
                pack: false,
                watch: false,
                warn: false
            },
            build: "common.lib+source"
        },
        "user_production": {
            options: {
                output: "public/js/build.user.js",
                pack: true,
                watch: false,
                warn: true
            },
            build: "common.lib+source"
        }
    };
    var optionsForMochaTest = {
        all: {
            options: {
                bail: true,
                reporter: 'spec',
                timeout: 999999999
            },
            src: ['test/*.test.js']
        },
        specific: {
            options: {
                reporter: 'spec',
                grep: "perf"
            },
            src: ['test/*.test.js']
        }
    };
    var optionsForEnv = {
        mocha: {
            NODE_ENV: 'mocha'
        }
    };
    var optionsForNodeInspector = {
        develop: {
            options: {
                "web-port": 8080,
                "debug-port": 5858,
                hidden: [
                    "bower_components",
                    "public",
                    "client"
                ]
            }
        }
    };
    var optionsForConcurrent = {
        options: {
            logConcurrentOutput: true
        },
        develop: {
            tasks: [
                "nodemon:develop",
                "watch"
            ]
        },
        production: {
            tasks: [
                "nodemon:production",
                "watch"
            ]
        }
    };
    var optionsForLess = {
        build: {
            options: {
                strictMath: true,
                compress: true,
                outputSourceFiles: false
            },
            files: {
                "public/css/custom.zzzik.bfc.css": [
                    "less/bfc.less"
                ],
                "public/css/primebrush.css": [
                    "bower_components/toastr/toastr.css",
                    "bower_components/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css",
                    "bower_components/select2/select2.css",
                    "bower_components/select2/select2-bootstrap.css",
                    "bower_components/fontawesome/css/font-awesome.css",
                    "less/bootstrap-theme.less"
                ]
            }
        }
    };
    var optionsForJshint = {
        options: {
            reporter: require("jshint-stylish"),
            jshintrc: true
        },
        client: [
            "client/config/**/**.js",
            "client/local_components/**/**.js",
            "client/**/**.js"
        ],
        server: [
            "app/**/**.js",
            "liv/**/**.js",
            "config/**/**.js",
            "server.js"
        ],
        other: [
            "Gruntfile.js"
        ]
    };
    require("load-grunt-tasks")(grunt);
    //
    grunt.initConfig({
        clean: optionsForClean,
        "node-inspector": optionsForNodeInspector,
        watch: optionsForWatch,
        nodemon: optionsForNodemon,
        yate: optionsForYate,
        lmd: optionsForLmd,
        mochaTest: optionsForMochaTest,
        env: optionsForEnv,
        concurrent: optionsForConcurrent,
        less: optionsForLess,
        jshint: optionsForJshint,
        migrations: optionsForMongooseMigrations
    });
    //
    grunt.registerTask("up:develop", ["build", "concurrent:develop"]);
    grunt.registerTask("up:production", ["build:production", "concurrent:production"]);
    //
    grunt.registerTask("test", ["env:mocha", "mochaTest:all"]);
    //
    grunt.registerTask("build", ["clean", "yate", "less", "lmd:user_develop"]);
    grunt.registerTask("build:production", ["clean", "yate", "less", "lmd:user_production"]);
    //
    grunt.registerTask("default", ["up:develop"]);
};
