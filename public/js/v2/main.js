$( document ).ready(function() {


    /* open mobile menu */
    $('.show-menu').on('click', function(e) {
        e.preventDefault();
        $('.header').toggleClass('active');
    });


    /*clean home slider */
	$('.clean_house .articles-slider').slick({
	  infinite: false,
	  dots: true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
	});

    /* crop articles */

    $(".crop-me").each(function(i){
        len=$(this).text().length;
        textLength = $(this).parents('.crop-text').data('text-length');
        if(len>textLength)
        {
          $(this).text($(this).text().substr(0,textLength)+'...');
        }
    });       



     /* Replace all SVG images with inline SVG */
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
});


$(window).scroll(function(){
 scroll = $(window).scrollTop();
  if (scroll >= 1) {
    $('.header').addClass('fixed');
  }
  else  {
     $('.header').removeClass('fixed');
  }
});