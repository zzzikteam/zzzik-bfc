"use strict";
var config = require("config"),
    locomotive = require("locomotive"),
    Controller = locomotive.Controller,
    async = require("async"),
    UseCases = require("../models/UseCase"),
    c = new Controller(),
    project = config.project,
    cheerio = require("cheerio"),
    Storage = require("../../lib/bluemix-storage"),
    _ = require("underscore"),
    templateDataDefaults = config.templateData;
//
var getTemplatePath = function(templateName) {
    return "./" + project + "/" + templateName;
};
var getMenuBySubMenu = function(id) {
    var menuItems = config.ui.useCasesMenu,
        res = _.find(menuItems, function(menuItem) {
            return _.some(menuItem.sub, function(subMenuItem) {

                return subMenuItem.id === id;
            });
        });
    return res.id;
};

c.index = function() {
    var tasks = [],
        self = this,
        responseHandler,
        reqDataFilter = {
            dPublishAt: {
                $lte: new Date()
            }
        };
    //
    responseHandler = function(err, templateData) {
        if (err) {
            return console.log(err);
        }
        self.render(getTemplatePath("/use-cases"), templateData);
    };
    //
    tasks.push(function(cb) {
        UseCases.find(reqDataFilter).sort({ dPublishAt: -1 }).exec(cb);
    });
    tasks.push(function(useCases, cb) {
        _.each(useCases, function(useCase) {
            useCase.tagsStr = useCase.tagsStr || "";
            if (useCase.tags) {
                useCase.tagsStr = useCase.tags.join(" ");
            }
            _.each(useCase.tags, function(tag) {
                if (!!tag) {
                    useCase.tagsStr = useCase.tagsStr + " " + getMenuBySubMenu(tag);
                }

            });
        });

        cb(null, useCases);
    });
    tasks.push(function(useCases, cb) {
        var templateData = _.extend(templateDataDefaults, {
            useCases: useCases
        });
        cb(null, templateData);
    });
    tasks.push(function(templateData, cb) {
        var menu = [],
            submenus = [],
            ui = config.ui.useCasesMenu;

        _.each(ui, function(elem) {
            menu.push({
                id: elem.id,
                name: elem.name
            });
        });
        _.each(ui, function(elem) {
            submenus.push({

                tags: elem.sub
            });
        });
        templateData.useCasesMenu = ui;
        templateData.menu = menu;
        templateData.submenus = submenus;
        cb(null, templateData);
    });
    //
    async.waterfall(tasks, responseHandler);
};

// TODO
// move under another endpoint
c.update = function() {
    var id = this.param("id"),
        req = this.req,
        file = req.files.file,
        fileName = file.name,
        filePathFrom = file.path,
        filePathTo = "/" + id + "/" + fileName,
        storage = new Storage();
    storage.container("bfc-pro-use-cases").upload(filePathFrom, filePathTo, this.send.bind(this));
};
c.show = function() {
    var tasks = [],
        self = this,
        responseHandler,
        id = this.param("id");
    responseHandler = function(err, templateData) {
        if (err) {
            return console.log(err);
        }
        self.render(getTemplatePath("/use-case"), templateData);
    };
    tasks.push(async.apply(UseCases.findById.bind(UseCases), id));
    tasks.push((useCase, cb) => {
        var $ = cheerio.load(useCase.code);
        if ($(".main-container").length > 0) {
            useCase.code = $(".main-container").html();
        }
        cb(null, useCase);
    });
    tasks.push(function(useCase, cb) {
        var html = useCase.code,
            $ = cheerio.load(html),
            $els = $("[src]");
        $els.each((index, elem) => {
            var $el = $(elem),
                urlBase = "../store/bfc-pro-use-cases/" + id + "/",
                urlBefore = $el.attr("src"),
                urlAfter = "";
            if (!urlBefore) return;
            if (urlBefore.match("/")) {
                urlAfter = urlBefore.replace(/.+\//, urlBase);
            } else {
                urlAfter = urlBase + urlBefore;
            }
            $el.attr("src", urlAfter);
            useCase.code = $.html();
        });
        cb(null, useCase);
    });
    tasks.push(function(useCase, cb) {
        var templateData = _.extend(templateDataDefaults, {
            useCase: useCase
        });
        cb(null, templateData);
    });
    async.waterfall(tasks, responseHandler);
};

c.before("show", function(req, res, cb) {
    var id = req.param("id");
    UseCases.findById(id).exec(function(err, useCase) {
        if (!err && useCase) {
            cb();
        } else {
            res.redirect("/404");
        }
    });
});
module.exports = c;
