"use strict";

var config = require("config"),
    locomotive = require("locomotive"),
    Controller = locomotive.Controller,

    c = new Controller(),
    project = config.project,
    getTemplatePath = function(templateName) {
        return "bfc/" + templateName;
    },
    _ = require("underscore"),
    templateDataDefaults = config.templateData;

//

c.index = function() {
    this.render(getTemplatePath("index"), templateDataDefaults);
};

c.notFound = function() {
    this.__req.res.status(404);
    this.render(getTemplatePath("404"), templateDataDefaults);
};

c.buy = function() {
    this.render(getTemplatePath("buy"), templateDataDefaults);
};
c.innovation = function() {
    this.render(getTemplatePath("innovation"), templateDataDefaults);
};
c.gPDSafeFood = function() {
    var templateData = _.defaults({
        theme: "blues"
    }, templateDataDefaults);
    this.render(getTemplatePath("gpd_safe_food"), templateData);
};
c.farm = function() {
    var templateData = _.defaults({
        theme: "blues"
    }, templateDataDefaults);
    this.render(getTemplatePath("farm"), templateData);
};
c.productionDistribution = function() {
    var templateData = _.defaults({
        theme: "blues"
    }, templateDataDefaults);
    this.render(getTemplatePath("production_distribution"), templateData);
};
c.gpd3 = function() {
    var templateData = _.defaults({
        theme: "blues"
    }, templateDataDefaults);
    this.render(getTemplatePath("gpd-3"), templateData);
};

c.med = function() {
    this.render(getTemplatePath("med"), templateDataDefaults);
};

c.acsg = function() {
    this.render(getTemplatePath("acsg"), templateDataDefaults);
};

/*c.health = function() {
    this.render(getTemplatePath("health"), templateDataDefaults);
};*/

c.news = function() {
    this.render(getTemplatePath("news"), templateDataDefaults);
};

c.article = function() {
    this.render(getTemplatePath("article"), templateDataDefaults);
};

c.naturalMakeUp = function() {
    var templateData = _.defaults({
        theme: "aspirin"
    }, templateDataDefaults);
    this.render(getTemplatePath("/natural-make-up"), templateData);
};

c.problemSolving = function() {
    this.render(getTemplatePath("/problem-solving"), templateDataDefaults);
};

c.product = function() {
    this.render(getTemplatePath("/product"), templateDataDefaults);
};

c.productsOfCleaning = function() {
    this.render(getTemplatePath("/products-of-cleaning"), templateDataDefaults);
};



c.safety = function() {
    this.render(getTemplatePath("/safety"), templateDataDefaults);
};

c.bodyCare = function() {
    this.render(getTemplatePath("/body_care"), templateDataDefaults);
};

c.duo = function() {
    this.render(getTemplatePath("/duo"), templateDataDefaults);
};

c.professional = function() {
    this.render(getTemplatePath("/professional"), templateDataDefaults);
};

c.homeCleaning = function() {
    var templateData = _.defaults({
        layout: "main-v2"
    }, templateDataDefaults);
    this.render(
        getTemplatePath("/home_cleaning"), templateData
    );
};
c.animalCare = function() {
    this.render(getTemplatePath("/animal_care"), templateDataDefaults);
};

c.airClean = function() {
    this.render(getTemplatePath("/air_clean"), templateDataDefaults);
};

c.proActive = function() {
    this.render(getTemplatePath("/pro_active"), templateDataDefaults);
};

c.cleaningComponents = function() {
    var templateData = _.defaults({
        theme: "buddha"
    }, templateDataDefaults);
    this.render(getTemplatePath("/cleaning-components"), templateData);
};
c.about = function() {
    this.render(getTemplatePath("/about"), templateDataDefaults);
};

c.probiotic = function() {
    var templateData = _.defaults({
        theme: "buddha"
    }, templateDataDefaults);
    this.render(getTemplatePath("/probiotic"), templateData);
};
c.directions = function() {
    this.render(getTemplatePath("/directions"), templateDataDefaults);
};

c.products = function() {
    this.render(getTemplatePath("/products"), templateDataDefaults);
};

c.private = function() {
    this.render(getTemplatePath("/private"), templateDataDefaults);
};

c.horeca = function() {
    this.render(getTemplatePath("/horeca"), templateDataDefaults);
};

//
module.exports = c;
