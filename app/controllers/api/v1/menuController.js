"use strict";
var config = require("config"),
    lcm = require("locomotive"),
    Controller = lcm.Controller,
    c = new Controller(),
    async = require("async");

c.index = function() {
    var data = config.ui.useCasesMenu;

    this.send(null, data);
};

module.exports = c;
