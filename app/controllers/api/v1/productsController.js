"use strict";
var config = require("config"),
    lcm = require("locomotive"),
    Controller = lcm.Controller,
    c = new Controller(),
    async = require("async"),
    request = require("request"),
    _ = require("underscore");

c.index = function() {
    var saveTraffic = false,
        data = [{
            "id": "1000117",
            "name": "Продукт 1",
            "descr": "",
            "fSort": 19,
            "fPrice": 0,
            "rState": 2,
            "sArticle": "",
            "rCategory": 27,
            "sTemplate": "<style>\n    div.remove-smells {\n        height: 41em;\n        width: 50%;\n        background: black;\n        background: url(\"<%= filebg %>\") no-repeat;\n        background-size: 100%;\n    }\n</style>\n<div class=\"remove-smells flip-container\">\n    <div class=\"flipper\">\n        <div class=\"front\">\n            <i></i>\n        </div>\n        <div class=\"back\">\n            <i></i>\n        </div>\n    </div>\n</div>",
            "rAccessory": null
        }, {
            "id": "1000120",
            "name": "Продукт 2",
            "descr": "",
            "fSort": 19,
            "fPrice": 0,
            "rState": 2,
            "sArticle": "",
            "rCategory": 27,
            "sTemplate": "<style>\n    div.remove-smells {\n        height: 41em;\n        width: 50%;\n        background: black;\n        background: url(\"<%= filebg %>\") no-repeat;\n        background-size: 100%;\n    }\n</style>\n<div class=\"remove-smells flip-container\">\n    <div class=\"flipper\">\n        <div class=\"front\">\n            <i></i>\n        </div>\n        <div class=\"back\">\n            <i></i>\n        </div>\n    </div>\n</div>",
            "rAccessory": null
        }],
        self = this,
        aTasks = [],
        url = "http://store.bfc-pro.com/api/content/goods?page=1&per_page=100";
    //
    if (saveTraffic) {
        aTasks.push(function(cb) {
            cb(null, data);
        });

    } else {
        aTasks.push(function(cb) {
            request.get(url, cb);
        });
        aTasks.push(function(res, resBody, cb) {
            var parsed = JSON.parse(resBody);
            cb(null, parsed);
        });
        aTasks.push(function(products, cb) {
            var filtered = _.filter(products, function(product) {
                return product.id === product.rAccessory || !product.rAccessory;
            });
            cb(null, filtered);
        });
    }

    async.waterfall(aTasks, self.send.bind(self));
};

module.exports = c;
