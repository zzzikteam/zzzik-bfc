"use strict";
var postmark = require("postmark"),
    client = new postmark.Client("6dc3aad8-0f25-42e1-91a2-f4aecc46635c"),
    lcm = require("locomotive"),
    Controller = lcm.Controller,
    c = new Controller(),
    async = require("async");

c.create = function() {
    var tasks = [],
        self = this;
    tasks.push(function(cb) {
        client.sendEmail({
            "From": "rogov.dmitry@gmail.com",
            "To": "rogov.dmitry@gmail.com",
            "Subject": "Test",
            "TextBody": "Hello from Postmark!"
        }, cb);
    });
    async.waterfall(tasks, function(data, cb) {
        cb();
        self.send(null, data);
    });
};

module.exports = c;
