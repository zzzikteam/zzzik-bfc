var mongoose = require("mongoose"),
    Schema   = mongoose.Schema,
    s;
//
s = new Schema({
	name:          {
		type:     String,
		required: true
	},
	address:       {
		type:     String,
		required: true
	},
	url:           {
		type: String
	},
	lat:           {
		type:     Number,
		required: true
	},
	lng:           {
		type:     Number,
		required: true
	},
	email:         {
		type: String
	},
	schedule:      {
		type: []
	},
	phone:         {
		type: String
	},
	transportType: {
		required: true,
		type:     String,
		enum:     [
			"auto",
			"commercial",
			"bus",
			"truck"
		]
	},
	brands:        {
		required: true,
		type:     [String]
	},
	brand:         {
		required: true,
		type:     String
	}
});
module.exports = mongoose.model("service-points", s);
