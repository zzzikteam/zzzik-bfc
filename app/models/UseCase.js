"use strict";
var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    s;
//
s = new Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    name: {
        type: String
    },
    _idYoutube: {
        type: String
    },
    code: {
        type: String
    },
    tags: {
        type: [String]
    },
    image: {
        type: String
    }
});
module.exports = mongoose.model("use-cases", s);
