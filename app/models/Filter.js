var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    schema,
    schemaEntry;
schemaEntry = new Schema({
    date: {
        type: Date
    },
    email: {
        type: String
    },
    lat: {
        type: String
    },
    lng: {
        type: String
    },
    address: {
        type: String
    },
    brand: {
        type: String
    },
    transportType: {
        type: String
    }
});
schema = new Schema({
    _id: {
        type: String
    },
    entries: [schemaEntry]
});
module.exports = mongoose.model("filter", schema);
